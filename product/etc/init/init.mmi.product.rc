# common initialization
on early-init
    # RAM boost 2.0
    setprop ro.config.moto_swap_supported true
    setprop ro.config.use_common_max_apps true

# common initialization
# Config low memory kill params
on post-fs
    setprop ro.lmk.file_low_percentage 30
    setprop ro.lmk.file_high_percentage 70
    setprop ro.lmk.pgscan_limit 3000
    setprop ro.lmk.swap_free_low_percentage 10
    setprop ro.lmk.swap_util_max 90
    setprop ro.lmk.thrashing_limit 50
    setprop ro.lmk.min_thrashing_limit 10
    setprop ro.lmk.thrashing_limit_decay 25
    setprop ro.lmk.thrashing_limit_critical 50
    setprop ro.lmk.threshold_decay 50
    setprop ro.lmk.filecache_min_kb 300000
    setprop ro.lmk.stall_accelerated_decay 10
    setprop ro.lmk.stall_accelerated_limit 40
    # LMK 3.0
    setprop ro.lmk.use_moto_strategy true
    setprop ro.lmk.kswapd_limit 90
    setprop ro.lmk.kswapd_limit_decay 10
    setprop ro.lmk.freeze_min_adj 0
    setprop ro.lmk.stall_limit_freeze 8
    setprop ro.lmk.critical_min_adj 201
    setprop ro.lmk.stall_limit_critical 4
    setprop ro.lmk.medium_min_adj 920
    setprop ro.lmk.psi_partial_stall_ms 50
    setprop ro.lmk.psi_complete_stall_ms 150
    setprop ro.lmk.kill_heaviest_task true
    setprop ro.lmk.kill_timeout_ms 100
    setprop ro.lmk.always_bg_cpuset_threads kcompactd0
    setprop ro.lmk.always_hfg_cpuset_threads system_server
    setprop ro.lmk.always_hbg_cpuset_threads kswapd0
    setprop persist.lmk.debug true
    #Dalvik configuration
    setprop dalvik.vm.dex2oat-threads 4
    setprop dalvik.vm.dex2oat-cpu-set 2,3,4,5
    setprop persist.sys.install.dex2oat-cpu-set 3,4,5,6
    # App compactor
    setprop ro.config.use_compaction true
    setprop ro.config.compact_action_2 2
    setprop ro.config.compact_bootcompleted true
    # ioPrefetch
    setprop persist.sys.ioprefetcher true

    # LowMemoryDetector of Framework
    setprop ro.lowmemdetector.psi_low_stall_us 50000
    setprop ro.lowmemdetector.psi_medium_stall_us 150000
    setprop ro.lowmemdetector.psi_high_stall_us 200000
    setprop ro.config.use_freezer true
    # Enable Qcom perf framework
    setprop persist.sys.perf_fwk_enabled true
   # do not pin dex files
    setprop ro.config.donot_pin_dex false
    setprop persist.device_config.runtime_native.usap_pool_enabled false
    #disable no_kill_cached process after post boot
    setprop ro.config.no_kill_duration_post_boot 0
    setprop debug.sf.boost_sf_on_touch true
    setprop persist.sys.touch_boost_enable true
    setprop persist.sys.moto_boost_enabled true
    setprop persist.sys.sched_booster_enabled true
    # moto app compact strategy
    setprop persist.sys.moto_cache_strategy true
    # moto screen off dexopt
    setprop persist.sys.screenoff_dexopt true
    # moto scroller
    setprop ro.config.use_moto_scroller false

    # max starting in bg, can be 1 in low ram device.
    setprop ro.config.max_starting_bg 8
    # use psi avg10 for mempressure in fwk to avoid ping-pong.
    setprop ro.config.use_psi_avg10_for_mempressure true
    # delay longer for service restart, will be rescheduled immediately once mempressure backing to normal.
    setprop ro.config.svc_restart_delay_on_moderate_mem 60000
    setprop ro.config.svc_restart_delay_on_low_mem 3600000
    setprop ro.config.svc_restart_delay_on_critical_mem 3600000
    # set input dispatching ANR timeout
    setprop persist.sys.dispatch_timeout_multiplier 2
    setprop persist.sys.service_timeout 40000
    setprop persist.sys.service_bg_timeout 400000
    setprop persist.sys.broadcast_fg_timeout 20000
    setprop persist.sys.broadcast_bg_timeout 120000
    setprop persist.sys.bind_timeout 36000

#Reinit lmkd to reconfigure lmkd propertise
on property:sys.boot_completed=1
    setprop lmkd.reinit 1

on property:sys.boot_completed=1
    write /dev/cpuset/top-app/cpus 0-7
    # Cpuset for boost
    write /dev/cpuset/boost-app/cpus 6-7
    write /dev/cpuset/system-background/cpus 0-3
    # Cpuset for foreground apps
    # do not change to 0-5, CTS failed on IKSWT-32275
    write /dev/cpuset/foreground/cpus 0-6
    write /dev/cpuset/h-foreground/cpus 0-7
    write /dev/cpuset/background/cpus 0-2
    write /dev/cpuset/h-background/cpus 0-6
    write /dev/cpuset/restricted/cpus 0-3

on  post-fs && property:ro.vendor.hw.ram=4GB
    setprop dalvik.vm.heapstartsize 8m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.75
    setprop dalvik.vm.heapminfree 512k
    setprop dalvik.vm.heapmaxfree 8m
    setprop ro.config.compact_action_1 2
    setprop ro.lmk.stall_limit_medium 0
on  post-fs && property:ro.vendor.hw.ram=6GB
    setprop dalvik.vm.heapstartsize 12m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 6m
    setprop dalvik.vm.heapmaxfree 24m
    setprop ro.config.compact_action_1 4
    setprop ro.lmk.stall_limit_medium 1
on  post-fs && property:ro.vendor.hw.ram=8GB
    setprop dalvik.vm.heapstartsize 16m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 8m
    setprop dalvik.vm.heapmaxfree 32m
    setprop ro.config.compact_action_1 4
    setprop ro.lmk.stall_limit_medium 1
on  post-fs && property:ro.vendor.hw.ram=12GB
    setprop dalvik.vm.heapstartsize 16m
    setprop dalvik.vm.heapgrowthlimit 256m
    setprop dalvik.vm.heapsize 512m
    setprop dalvik.vm.heaptargetutilization 0.5
    setprop dalvik.vm.heapminfree 8m
    setprop dalvik.vm.heapmaxfree 32m
    setprop ro.config.compact_action_1 4
    setprop ro.lmk.stall_limit_medium 1

# enable perf checkin
on  post-fs && property:ro.product.is_production=false
    setprop debug.sf.track_jank_threshold 5
    setprop persist.sys.moto_perf_monitor true
on  post-fs && property:ro.product.is_production=true
    setprop persist.sys.moto_perf_monitor false

on property:sys.boot_completed=1 && property:ro.vendor.zram.product_swapon=true
    trigger sys-boot-completed-set

on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=false
    swapon_all /vendor/etc/fstab.qcom.zram
on sys-boot-completed-set && property:persist.sys.zram_wb_enabled=true
    swapon_all /vendor/etc/fstab.qcom.zramwb

on property:ro.product.cpu.abi=arm64-v8a
    setprop dalvik.vm.dex2oat64.enabled true

on boot
    #LHDC
    setprop persist.mot_bt.lhdc_enable true

on property:ro.product.is_production=false
    setprop log.tag.debugtouch D
    setprop log.tag.debugkey D

on property:ro.carrier="reteu2"
    setprop ro.setupwizard.require_network any
    setprop ro.setupwizard.user_req_network any